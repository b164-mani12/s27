const http = require(`http`);

const port = 6000;

//mock database
let directory = [
	{
		"name" : "Brandon",
		"email": "brandon@mail.com",
	},
	{
		"name" : "Jobert",
		"email": "jobert@mail.com",	
	}
]

console.log(typeof directory);

const server = http.createServer((request, response) => {
	if(request.url == '/users' && request.method === "GET"){
		response.writeHead(200, {'Content-Type': 'application/json'});
		response.write(JSON.stringify(directory));
		response.end();
	}
	if(request.url =='/users' && request.method == "POST"){
		let requestBody = '';
		
		request.on('data', function(data){
			requestBody += data;

		});

		request.on('end', function(){
			console.log(typeof requestBody);
			requestBody = JSON.parse(requestBody);

			let newUser = {
				"name": requestBody.name,
				"email": requestBody.email
			}

			directory.push(newUser);
			console.log(directory);

			response.writeHead(200,{'Content-Type': 'application/json'});
			response.write(JSON.stringify(newUser));
			response.end();
		})
	}	

});

server.listen(port);

console.log(`Server now accessible at localhost: ${port}.`);