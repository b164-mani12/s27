const http = require(`http`);

const port = 3000;

//mock database
let users = [
			{
	                "firstName": "Mary Jane",
	                "lastName": "Dela Cruz",
	                "mobileNo": "09123456789",
	                "email": "mjdelacruz@mail.com",
	                "password": 123
	        },
	        {
	                "firstName": "John",
	                "lastName": "Doe",
	                "mobileNo": "09123456789",
	                "email": "jdoe@mail.com",
	                "password": 123
	        }
]

const server = http.createServer((request, response) => {
	if(request.url == '/profile' && request.method === "GET"){
		response.writeHead(200, {'Content-Type': 'application/json'});
		response.write(JSON.stringify(users));
		response.end();
	}
	if(request.url =='/NewUsers' && request.method == "POST"){
		let addUser = '';
		
		request.on('data', function(data){
			addUser += data;

		});

		request.on('end', function(){
			addUser = JSON.parse(addUser);

			let newUser = {
					"firstName": addUser.firstName,
	                "lastName": addUser.lastName,
	                "mobileNo": addUser.mobileNo,
	                "email": addUser.email,
	                "password": addUser.password
			}

			users.push(newUser);
			console.log(users);

			response.writeHead(200,{'Content-Type': 'application/json'});
			response.write(JSON.stringify(newUser));
			response.end();
		})
	}	

});

server.listen(port);

console.log(`Server now accessible at localhost: ${port}.`);